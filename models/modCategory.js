const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize.js');

class Category extends Sequelize.Model {}

Category.init({
    category_name: Sequelize.STRING
}, {
    sequelize,
    modelName: 'category'
});

module.exports = Category;