const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize.js');

class Order extends Sequelize.Model {}

Order.init({
    invoice_number: Sequelize.INTEGER,
    transaction_date: Sequelize.DATE,
    total_pay: Sequelize.INTEGER,
    pay: Sequelize.INTEGER,
    change: Sequelize.INTEGER,
    information: Sequelize.TEXT
}, {
    sequelize,
    modelName: 'order'
});

module.exports = Order;