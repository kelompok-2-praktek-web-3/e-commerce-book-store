const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize.js');

class User extends Sequelize.Model {}

User.init({
    name: Sequelize.STRING,
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    role: Sequelize.STRING
}, {
    sequelize,
    modelName: 'user'
});

module.exports = User;