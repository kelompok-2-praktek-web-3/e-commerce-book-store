const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize.js');

class OrderDetail extends Sequelize.Model {}

OrderDetail.init({
    quantity: Sequelize.INTEGER,
    sub_total: Sequelize.DOUBLE
}, {
    sequelize,
    modelName: 'orderDetail'
});

module.exports = OrderDetail;