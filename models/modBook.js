const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize.js');

class Book extends Sequelize.Model {}

Book.init({
    book_title: Sequelize.STRING,
	author: Sequelize.STRING,
	publisher: Sequelize.STRING,
	num_pages: Sequelize.STRING,
	picture: Sequelize.STRING,
	price: Sequelize.INTEGER,
	description: Sequelize.STRING,
	release_date: Sequelize.DATE
}, {
    sequelize,
    modelName: 'book'
});

module.exports = Book;