const express = require('express');
const app = express.Router();

const contBook = require('../controllers/contBook.js');
const contCategory = require('../controllers/contCategory.js');
const contUser = require('../controllers/contUser.js');
const contOrder = require('../controllers/contOrder.js');

const auth = require('../configs/auth.js');

//-------------------------------------------------------------------------------------------------------------------

app.get('/', (req, res) => {
    res.render('index.ejs');
})

app.get('/user', auth.verifyToken, contUser.getIndexUser);
app.post('/user/register', contUser.postRegister);
app.post('/user/login', contUser.postLogin);
app.put('/user/update/:id', contUser.updateUser);

app.get('/book', contBook.getAllBook);
app.get('/book/detail/:id', contBook.getDetailBook);
app.post('/book/create', auth.verifyToken, contBook.createBook);
app.put('/book/:id', auth.verifyToken, contBook.updateBook);
app.delete('/book/:id', auth.verifyToken, contBook.deleteBook);

app.get('/book/category', contCategory.getAllCategory);
app.post('/book/category/create', auth.verifyToken, contCategory.createCategory);
app.put('/book/category/:id', auth.verifyToken, contCategory.updateCategory);
app.delete('/book/category/:id', auth.verifyToken, contCategory.deleteCategory);

app.get('/order', auth.verifyToken, contOrder.getAllOrder);
app.get('/order/detail/:id', auth.verifyToken, contOrder.getOneOrder);
app.post('/order/create', auth.verifyToken, contOrder.createOrder);
app.put('/order/:id', auth.verifyToken, contOrder.updateOrder);
app.delete('/order/:id', auth.verifyToken, contOrder.deleteOrder);

app.use(function (req, res, next) {
    res.status(404).render('404Notfound.ejs');
});

module.exports = app;