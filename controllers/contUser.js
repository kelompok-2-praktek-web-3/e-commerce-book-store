const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
dotenv.config();

const user = require('../models/modUser.js');

// --------------------------------------------------------------------------------------------------------------------

// Untuk Melihat Index Data User
module.exports.getIndexUser = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            res.json({
                message: 'OK',
                authData: authData
            })
        }
    })
}

// Untuk Membuat Akun Baru
module.exports.postRegister = (req, res) => {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    user.findOrCreate({
        where: {
            email: req.body.email
        },
        defaults: {
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            password: hash,
            role: req.body.role
        }
    }).then((user) => {
        res.json(user);
    }).catch((error) => {
        console.log(error);
    })
}

// Untuk Melakukan Login
module.exports.postLogin = (req, res) => {
    user.findOne({
        where: {
            username: req.body.username
        }
    }).then((user) => {
        if (!user) {
            res.status(400).send('Username Not Found');
        }
        bcrypt.compare(req.body.password, user.get('password'), function (err, isMatch) {
            if (err) {
                res.status(400).send('Password Error');
            };

            if (isMatch) {
                jwt.sign({
                    id: user.get('id'),
                    username: user.get('username'),
                    role: user.get('role')
                }, process.env.SECRETKEY, (err, token) => {
                    res.json ({
                        token: token
                    })
                })
            } else {
                res.status(400).send('Wrong Password');
            }
        })
    })
}

// Untuk Meng-Update Data User
module.exports.updateUser = (req, res) => {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    user.findOne({
        where: {
            id: req.params.id
        }
    }).then((admin) => {
        if (user) {
            user.update({
                name: req.body.name,
                username: req.body.username,
                email: req.body.email,
                password: hash,
                role: req.body.role
            }).then((user) => {
                res.json(user);
            })
        }
    })
}