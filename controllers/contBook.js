const book = require('../models/modBook.js');
const category = require('../models/modCategory.js');

const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
dotenv.config();

// -------------------------------------------------------------------------------------------------------------------

book.belongsTo(category);

//---------------------------------------------------------------------------------------------------------------------


// Untuk Menampilkan Semua Data Buku
module.exports.getAllBook = (req, res) => {
 
            book.findAll().then((book) => {
                res.json({
                    message: 'OK',
                    book
                });
            })
}

// Untuk Menampilkan Detail Satu Buku
module.exports.getDetailBook = (req, res) => {
        book.findOne({
            where:{
                id: req.params.id
            }
        }).then((book) => {
            res.json(book);
    })
}

// Untuk Meng-input Buku Baru
module.exports.createBook = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (error, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                book.create({
                    book_title: req.body.book_title,
                    author: req.body.author,
                    publisher: req.body.publisher,
                    num_pages: req.body.num_pages,
                    picture: req.body.picture,
                    price: req.body.price,
                    description: req.body.description,
                    release_date: req.body.release_date
                }).then((book) => {
                    res.json(book);
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}

// Untuk Meng-update Data Buku
module.exports.updateBook = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (error, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                book.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then((book) => {
                    if (book) {
                        book.update({
                            book_title: req.body.book_title,
                            author: req.body.author,
                            publisher: req.body.publisher,
                            num_pages: req.body.num_pages,
                            picture: req.body.picture,
                            price: req.body.price,
                            description: req.body.description,
                            release_date: req.body.release_date
                        }).then((book) => {
                            res.json(book)
                        })
                    }
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}

// Untuk Menghapus Buku
module.exports.deleteBook = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (error, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                book.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then((book) => {
                    res.json(book)
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}