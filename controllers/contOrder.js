const order = require('../models/modOrder.js')
const orderDetail = require('../models/modOrderDetail.js')
const user = require('../models/modUser.js')
const book = require('../models/modBook.js')

const bcrypt = require('bcryptjs')
const dotenv = require('dotenv')
const jwt = require('jsonwebtoken')
dotenv.config();

order.belongsTo(user)
order.hasOne(orderDetail)
orderDetail.belongsTo(book)

// --------------------------------------------------------------------------------------------------------------------

// Digunakan untuk Menampilkan Semua Data Order
module.exports.getAllOrder = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                order.findAll().then((order) => {
                    res.json({
                        message: 'OK',
                        authData: authData,
                        order
                    });
                })
            }
        }
    })
}

// Digunakan untuk Menampilkan Satu Order
module.exports.getOneOrder = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            order.findOne({
                    where: {
                        id: req.params.id
                    }
                })
                .then((order) => {
                    res.json({
                        message: 'OK',
                        authData: authData,
                        order
                    });
                })
        }
    })
}

// DIgunakan untuk Menambahkan Order Baru
module.exports.createOrder = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            order.create({
                invoice_number: req.body.invoice_number,
                transaction_date: req.body.transaction_date,
                information: req.body.information
            }).then((order) => {
                res.json(order);
            })
        }
    })

}

// Digunakan untuk Meng-update Order
module.exports.updateOrder = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            orderDetail.create({
                invoice_number: req.body.invoice_number,
                price: req.body.price,
                qty: req.body.qty,
                subtotal: req.body.subtotal
            }).then((orderDetail) => {
                order.findOne({
                    where: {
                        invoice_number: req.body.invoice_number
                    }
                }).then((order) => {
                    if (order) {
                        order.update({
                            total_pay: req.body.total_pay,
                            pay: req.body.pay,
                            change: req.body.change
                        }).then((order) => {
                            res.json({
                                order: order,
                                orderDetail: orderDetail
                            })
                        })
                    }
                })
            })
        }
    })
}

// Digunakan untuk Menghapus Order
module.exports.deleteOrder = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            order.destroy({
                where: {
                    id: req.params.id
                }
            }).then((order) => {
                res.json(order)
            })
        }
    })
}