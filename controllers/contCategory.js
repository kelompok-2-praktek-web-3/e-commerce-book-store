const category = require('../models/modCategory.js');

const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
dotenv.config();

//---------------------------------------------------------------------------------------------------------------------

// Untuk Menampilkan Semua Kategori Buku
module.exports.getAllCategory = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            book.findAll().then((category) => {
                res.json({
                    message: 'OK',
                    authData: authData,
                    category
                });
            })
        }
    })
}

// Untuk Menambahkan Kategori Baru
module.exports.createCategory = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                category.create({
                    category_name: req.body.category_name
                }).then((category) => {
                    res.json(category);
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}

// Untuk Meng-update Data Kategori
module.exports.updateCategory = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                category.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then((category) => {
                    if (category) {
                        category.update({
                            category_name: req.body.category_name
                        }).then((category) => {
                            res.json(category)
                        })
                    }
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}

// Untuk Menghapus Kategori
module.exports.deleteCategory = (req, res) => {
    jwt.verify(req.token, process.env.SECRETKEY, (err, authData) => {
        if (error) {
            res.sendStatus(403);
        } else {
            if (authData['role'] == 'admin') {
                category.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then((category) => {
                    res.json(category)
                })
            } else {
                res.json({
                    message: 'Error',
                    authData: authData,
                });
            }
        }
    })
}