const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
const path = require('path');
const router = require('./routes/router');
const sequelize = require('./configs/sequelize.js');

app.set('view engine', 'ejs');

app.use(
    bodyParser.urlencoded({
        extended: false
    })
)

app.use(
    '/static', express.static(
        path.join(
            __dirname, 'public'
        )
    )
)

app.use(bodyParser.json());

app.use(router);

app.listen(port, () => {
    console.log(`Port yang digunakan yaitu Port 3000`);
    sequelize.sync();
})